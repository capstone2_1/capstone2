const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CourseController = require('../controllers/CourseController')


//route for getting all the courses
router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses))
})

//route for getting a specific course
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
    CourseController.get({ courseId }).then(course => res.send(course)) 
})

//route for adding course
router.post('/', auth.verify, (req, res) => {
    CourseController.add(req.body).then(result => res.send(result))
})

//for editing a course
router.put('/', auth.verify, (req, res) => {
    CourseController.update(req.body).then(result => res.send(result))
})

//for reactivating a course
router.put('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
    CourseController.reactivate({ courseId }).then(result => res.send(result))
})

//for deleting a course
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
    CourseController.archive({ courseId }).then(result => res.send(result))
})

module.exports = router