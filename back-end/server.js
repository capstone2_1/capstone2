const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')

//const courseRoutes = require('./routes/course');
require('dotenv').config()

const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

//connect to mongoDB atlas database
const connectionString = process.env.DB_CONNECTION_STRING;
mongoose.connect(connectionString, { useNewUrlParser: true,
	useUnifiedTopology: true,
useFindAndModify: false});
mongoose.connection.once('open', () => console.log('You are now connected in MongoDB Atlas Database.'));


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//routes for the users and courses
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes)

app.listen(process.env.PORT || 3000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 3000 }`);
})  