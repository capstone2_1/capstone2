let formSubmit = document.querySelector("#createCourse");
formSubmit.addEventListener("submit", (e) => {
	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	let token = localStorage.getItem("token");

	fetch(`http://localhost:3000/api/courses`, {
		method: 'POST', 
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			price: coursePrice,
			description: courseDescription
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if (data === true) {
			alert("Added course successfully")
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong")
		}
	});
});

//this will check if the user is admin or non-admin
let adminProfile = document.querySelector("#adminProfile")
let userProfile = document.querySelector("#userProfile")

if(adminUser == "false" || !adminUser) {
  //if user is regular user, do not show add course button
  userProfile.innerHTML = 
    `
      
        <a href="./userProfile.html" class="dropdown-item"> Profile </a>
               
    `
} else {
  //display add course if user is an admin
  adminProfile.innerHTML =
    `
      <li class="nav-item">
        <a href="./adminProfile.html" class="dropdown-item"> Profile </a>
      </li>
    `
}