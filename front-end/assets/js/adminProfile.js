let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem("token");
// let courseId = params.get('courseId')

fetch(`http://localhost:3000/api/users/details`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    })
    .then((res) => res.json())

    .then((data) => {
        console.log(data);
        let myInfo = document.querySelector(`#myInfo`);
        let myName = document.querySelector(`.myName`);

        if (data) {
            myInfo.innerHTML =
                `
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">First Name</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${data.firstName}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Last Name</h6>
                                </div>
                                <div class="col-sm-9 text-secondary lastName">
                                    ${data.lastName}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Email</h6>
                                </div>
                                <div class="col-sm-9 text-secondary email">
                                    ${data.email}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Mobile Number</h6>
                                </div>
                                <div class="col-sm-9 text-secondary mobileNo">
                                    ${data.mobileNo}
                                </div>
                            </div>
                        </div>
                     </div>
                    <div class="row gutters-sm"> 
                        <div class="col-sm-5 mb-3">
                            <div class="card h-100">
                                <button class="btn" style="background-color: #344da9; color: white"><a style="color: white" href="./courses.html">All Courses</button>
                            </div>
                        </div>
                        
                    </div>
            `;
        }
        if (data) {
            myName.innerHTML = 
            `
            <h4>${data.firstName}</h4>
            `
        }
    });