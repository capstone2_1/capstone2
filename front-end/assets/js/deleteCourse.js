//logic for deleting a course
//soft delete only. The course is still in the database with an isActive:false

const deleteCourse = () => {
        // getting id from url
    let token = localStorage.getItem('token');
    const url_string = window.location.href; //window.location.href
    const url = new URL(url_string);
    const courseId = url.searchParams.get("courseId");

    fetch(`http://localhost:3000/api/courses/${courseId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            if(data === true){
                //redirect to course
                alert('Course is now deleted!')
                window.location.replace("./courses.html")
            } else {
                //redirect in creating course
                alert("Something went wrong")
            }
        })


console.log('deleting')
}

deleteCourse()