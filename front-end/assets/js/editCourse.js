let editCourse = document.querySelector("#editCourse")
let isAdmin = localStorage.getItem('isAdmin');

editCourse.addEventListener('submit', (e) => {
	e.preventDefault()

	let courseName = document.querySelector('#courseName').value;
	let courseDescription = document.querySelector('#courseDescription').value;
	let coursePrice = document.querySelector('#coursePrice').value;

	let token = localStorage.getItem('token');
	const url_string = window.location.href; //window.location.href
	const url = new URL(url_string);
	const courseId = url.searchParams.get("courseId");

	fetch('http://localhost:3000/api/courses', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId,
			name: courseName,
			description: courseDescription,
			price: coursePrice
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//redirect to course
				alert('Course is now up-to-date!')
				window.location.replace("./courses.html")
			} else {
				//redirect in creating course
				alert("Something went wrong")
			}
		})
})

//this will check if the user is admin or non-admin
let adminProfile = document.querySelector("#adminProfile")
let userProfile = document.querySelector("#userProfile")

if(adminUser == "false" || !adminUser) {
 
  userProfile.innerHTML = 
    `
      
        <a href="./userProfile.html" class="dropdown-item"> Profile </a>
               
    `
} else {
 
  adminProfile.innerHTML =
    `
      <li class="nav-item">
        <a href="./adminProfile.html" class="dropdown-item"> Profile </a>
      </li>
    `
}