//clear you local storage
//the logic here will logout the page
//this will just kill the current session
localStorage.clear();

//redirect to the login page
window.location.replace('../index.html');