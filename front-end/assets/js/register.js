//add an event to the register form
//this logic will register a unique user

let registerForm = document.querySelector(".registerUser")
console.dir(registerForm)

registerForm.addEventListener('submit', (e) => {
	e.preventDefault()
	let firstName = registerForm.elements.firstName.value;
	let lastName = registerForm.elements.lastName.value;
	let email = registerForm.elements.userEmail.value;
	let mobileNo = registerForm.elements.mobileNumber.value;
	let password1 = registerForm.elements.password1.value;
	let password2 = registerForm.elements.password2.value;

   // validation to enable submit button when all fields are populated and both passwords match and mobile is equall to 11
    if((password1 !== '' && password2 !== '' ) && (password2 === password1) && (mobileNo.length === 11)){

		//check for duplicate email in database first
		fetch('http://localhost:3000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		

		//if no duplicates found
		if (data === false) {
			fetch('http://localhost:3000/api/users/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					//Registration successful
					alert("registered successfuly")
					//redirect to login
					window.location.replace("./login.html");
				}else {
					alert("Something went wrong");
				}
			})
			.catch((error) => {
			  console.error('Error:', error);
			});
		}
		})

	}else {
		alert("Please check the information provided")
	}

})