// the user has a specific userProfile page


let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem("token");

// let courseId = params.get('courseId')

fetch(`http://localhost:3000/api/users/details`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    })
    .then((res) => res.json())

    .then((data) => {
        console.log(data);
        let myInfo = document.querySelector(`#myInfo`);
        let myName = document.querySelector(`.myName`);

        if (data) {
            myInfo.innerHTML =
                `
                    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <h6 class="mb-0">First Name</h6>
                </div>
                <div class="col-sm-9 text-secondary">
                    ${data.firstName}
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <h6 class="mb-0">Last Name</h6>
                </div>
                <div class="col-sm-9 text-secondary lastName">
                ${data.lastName}
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <h6 class="mb-0">Email</h6>
                </div>
                <div class="col-sm-9 text-secondary email">
                ${data.email}
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <h6 class="mb-0">Mobile Number</h6>
                </div>
                <div class="col-sm-9 text-secondary mobileNo">
                ${data.mobileNo}
                </div>
            </div>
        </div>
    </div>
    <div class="row gutters-sm">
                                <div class="col-sm-12 mb-3">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <h6 class="d-flex align-items-center mb-3">Courses Enrolled</h6>
                                            <ol id="subjectContainer"></ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5 mb-3">
                                    <div class="card h-100">
                                        
                                        <button class="btn" style="background-color: #344da9; color: white"><a style="color: white" href="./courses.html">View Available Courses</button>
                                        
                                    </div>
                                </div>
                            </div>

            `;
        myName.innerHTML = 
        `
        <h4>${data.firstName}</h4>
        `
            
        }
        

        //This function will loop and everytime the non admin users adds a course, all the added course will be displayed in the userProfile page
        let subjectData = data.enrollments;
        console.log(subjectData);
        let courseInfo = []; // empty array
        let subjectContainer = document.querySelector("#subjectContainer");
      
        for (let subject of subjectData) {
            console.log(subject.courseId);

            fetch(`http://localhost:3000/api/courses/${subject.courseId}`)
                .then((res) => res.json())
                .then((data) => {
                    console.log(data);
                    courseInfo.push({
                        courseName: data.name,
                        courseDescription: data.description,
                    }); 


                    subjectContainer.innerHTML = "";

                    for (let i = 0; i < courseInfo.length; i++) {
                        
                        const newLi = document.createElement("li");
                        newLi.append(
                            `${courseInfo[i].courseName} →  ${courseInfo[i].courseDescription}`
                        );
                        subjectContainer.append(newLi);
                    }
                });
        }


    });